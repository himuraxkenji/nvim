let s:white = {"gui": "#ABB2BF", "cterm": "145", "cterm16": "7"}

hi Comment cterm=italic
let g:gruvbox_hide_endofbuffer=1
let g:gruvbox_terminal_italics=1
let g:gruvbox_termcolors=256

syntax on
colorscheme gruvbox
"
" colorscheme molokai 


set termguicolors
" hi LineNr guibg=NONE
highlight Normal guibg=NONE

